// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_TDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SB_TDS, "SB_TDS" );

DEFINE_LOG_CATEGORY(LogSB_TDS)
 