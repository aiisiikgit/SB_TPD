// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SB_TDSGameMode.generated.h"

UCLASS(minimalapi)
class ASB_TDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASB_TDSGameMode();
};



